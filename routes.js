const express = require('express'), // permite ejecutar un servidor.
    formidable = require('formidable'), //permite recivibir archivos del front.
    cmd = require('node-cmd'), // permite ejecutar comando en node.
    Promise = require('bluebird'); // permite crear promesas.
const router = express.Router();

router.post('/up', function (req, res){
    const form = new formidable.IncomingForm();
    form.parse(req);
    form.on('fileBegin', function (name, file){
        const nameWhitoutSpaces= file.name.replace(/\s/g,"_");        
        file.path = __dirname + '/files/uploads/' + nameWhitoutSpaces;
    });
    form.on('file', function (name, file){
        console.log('Uploaded ' + file.name);
        res.json({response: file});
    });
});

router.post('/down', function(req, res){
    const getAsync = Promise.promisify(cmd.get, { multiArgs: true, context: cmd })
    // getAsync('soffice --convert-to odt --outdir ./downloads ./uploads/prueba.docx --headless').then(data => { 
    const nameWhitoutSpaces= req.body.name.replace(/\s/g,"_");
    getAsync('soffice --convert-to '+req.body.extOutput+' --outdir ./files/downloads ./files/uploads/'+nameWhitoutSpaces+'.'+req.body.extInput+' --headless')
    .then(data => {
        console.log('Converting... ' + req.body.name + ' of ' + req.body.extInput + ' to ' + req.body.extOutput);
        res.sendFile(__dirname + '/files/downloads/'+nameWhitoutSpaces+'.'+req.body.extOutput);
    }).catch(err => {
      console.log('cmd err', err)
    })
});

module.exports = router;