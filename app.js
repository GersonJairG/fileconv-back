const  express = require('express'), // permite ejecutar un servidor.
    cors = require('cors'), // permite hacer peticiones validando los cors.
    bodyParser = require('body-parser');//permite ver la data enviada desde el front parseada.
        
const routes = require('./routes');
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

//routes
app.use(routes);

app.listen(3000, () => {
    console.log('Server on port 3000');
});

